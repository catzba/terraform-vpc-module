output "subnet_public_id" {
  description = "List of IDs of instances"
  value       = local.subnet_public_id
}

output "subnet_public_id2" {
  description = "List of IDs of instances"
  value       = local.subnet_public_id2
}

output "subnet_private_id" {
  description = "List of IDs of instances"
  value       = local.subnet_private_id
}

output "subnet_private_id2" {
  description = "List of IDs of instances"
  value       = local.subnet_private_id2
}

output "vpc_id" {
  description = "List of IDs of instances"
  value       = local.vpc_id
}
