output "subnet_public_id" {
  description = "List of IDs of instances"
  value       = aws_subnet.public[0].id
}

output "subnet_public_id2" {
  description = "List of IDs of instances"
  value       = local.public2_id
}

output "subnet_private_id" {
  description = "List of IDs of instances"
  value       = aws_subnet.private[0].id
}

output "subnet_private_id2" {
  description = "List of IDs of instances"
  value       = local.private2_id
}

output "vpc_id" {
  description = "List of IDs of instances"
  value       = aws_vpc.vpc[0].id
}