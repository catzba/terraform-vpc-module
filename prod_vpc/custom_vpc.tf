#-----------------------------------------------------------------------------------------------------
# LOCALS
#-----------------------------------------------------------------------------------------------------
locals {
  vpc_enabled     = var.prod_vpc_enabled ? true : false
  av_zones_lenght = length(data.aws_availability_zones.available[0].names)
  public2_en      = local.av_zones_lenght >= 4 ? true : false
  private2_en     = local.av_zones_lenght >= 3 ? true : false
  private2_id     = local.private2_en ? aws_subnet.private2_inzone[0].id : aws_subnet.private2[0].id
  public2_id      = local.public2_en ? aws_subnet.public2_inzone[0].id : aws_subnet.public2[0].id
}

#-----------------------------------------------------------------------------------------------------
# VPC
#-----------------------------------------------------------------------------------------------------

resource "aws_vpc" "vpc" {
  count                            = local.vpc_enabled ? 1 : 0
  cidr_block                       = "10.0.0.0/16"
  instance_tenancy                 = "default"
  enable_dns_support               = true
  enable_dns_hostnames             = true
  enable_classiclink               = false
  assign_generated_ipv6_cidr_block = false
  enable_classiclink_dns_support   = false

  tags = {
    Name = "${var.project_name}-${var.environment}-vpc"
  }
}

#-----------------------------------------------------------------------------------------------------
# NAT GATEWAY + Elastic IP
#-----------------------------------------------------------------------------------------------------

resource "aws_eip" "nat" {
  count = local.vpc_enabled ? 1 : 0
  vpc   = true

  tags = {
    Name = "${var.project_name}-${var.environment}-nat_ip"
  }
}

resource "aws_nat_gateway" "n_gw" {
  count         = local.vpc_enabled ? 1 : 0
  allocation_id = aws_eip.nat[0].id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = "${var.project_name}-${var.environment}-nat_gw"
  }
}

#-----------------------------------------------------------------------------------------------------
# INTERNET GATEWAY
#-----------------------------------------------------------------------------------------------------

resource "aws_internet_gateway" "i_gw" {
  count  = local.vpc_enabled ? 1 : 0
  vpc_id = aws_vpc.vpc[0].id

  tags = {
    Name = "${var.project_name}-${var.environment}-i_gw"
  }
}

#-----------------------------------------------------------------------------------------------------
# Aviabilyty zones
#-----------------------------------------------------------------------------------------------------

data "aws_availability_zones" "available" {
  count = 1 # local.vpc_enabled ? 1 : 0 # this logic disable because this data use in locals and causes logic error
  state = "available"
}

#-----------------------------------------------------------------------------------------------------
# SUBNETS Privat & Public
#-----------------------------------------------------------------------------------------------------

resource "aws_subnet" "public" {
  count                   = local.vpc_enabled ? 1 : 0
  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available[0].names[0]

  tags = {
    Name = "${var.project_name}-${var.environment}-Public_subnet"
  }
}

resource "aws_subnet" "public2" {
  count                   = local.vpc_enabled && ! local.public2_en ? 1 : 0
  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available[0].names[1]

  tags = {
    Name = "${var.project_name}-${var.environment}-Public_subnet2"
  }
}

resource "aws_subnet" "public2_inzone" {
  count                   = local.vpc_enabled && local.public2_en ? 1 : 0
  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available[0].names[3]

  tags = {
    Name = "${var.project_name}-${var.environment}-Public_subnet2"
  }
}

resource "aws_subnet" "private" {
  count                   = local.vpc_enabled ? 1 : 0
  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available[0].names[1]

  tags = {
    Name = "${var.project_name}-${var.environment}-Private_subnet"
  }
}

resource "aws_subnet" "private2" {
  count                   = local.vpc_enabled && ! local.private2_en ? 1 : 0
  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available[0].names[0]

  tags = {
    Name = "${var.project_name}-${var.environment}-Private_subnet2"
  }
}

resource "aws_subnet" "private2_inzone" {
  count                   = local.vpc_enabled && local.private2_en ? 1 : 0
  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available[0].names[2]

  tags = {
    Name = "${var.project_name}-${var.environment}-Private_subnet2"
  }
}

#-----------------------------------------------------------------------------------------------------
# ROUTE TABLE Private from vpc aws_default_route_table + association
#-----------------------------------------------------------------------------------------------------

resource "aws_default_route_table" "private" { # route table
  count                  = local.vpc_enabled ? 1 : 0
  default_route_table_id = aws_vpc.vpc[0].default_route_table_id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.n_gw[0].id
  }

  tags = {
    Name = "${var.project_name}-${var.environment}-private_route"
  }
}

resource "aws_route_table_association" "private" { # association table with subnet
  count          = local.vpc_enabled ? 1 : 0
  subnet_id      = aws_subnet.private[0].id
  route_table_id = aws_default_route_table.private[0].id
}

resource "aws_route_table_association" "private2" { # association table with subnet
  count          = local.vpc_enabled && ! local.private2_en ? 1 : 0
  subnet_id      = aws_subnet.private2[0].id
  route_table_id = aws_default_route_table.private[0].id
}

resource "aws_route_table_association" "private2_inzone" { # association table with subnet
  count          = local.vpc_enabled && local.private2_en ? 1 : 0
  subnet_id      = aws_subnet.private2_inzone[0].id
  route_table_id = aws_default_route_table.private[0].id
}

#-----------------------------------------------------------------------------------------------------
# ROUTE TABLE Public + association
#-----------------------------------------------------------------------------------------------------

resource "aws_route_table" "public" {
  count  = local.vpc_enabled ? 1 : 0
  vpc_id = aws_vpc.vpc[0].id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.i_gw[0].id
  }

  tags = {
    Name = "${var.project_name}-${var.environment}-public_route"
  }
}

resource "aws_route_table_association" "public" { # association table with subnet
  count          = local.vpc_enabled ? 1 : 0
  subnet_id      = aws_subnet.public[0].id
  route_table_id = aws_route_table.public[0].id
}

resource "aws_route_table_association" "public2" { # association table with subnet
  count          = local.vpc_enabled && ! local.public2_en ? 1 : 0
  subnet_id      = aws_subnet.public2[0].id
  route_table_id = aws_route_table.public[0].id
}

resource "aws_route_table_association" "public2_inzone" { # association table with subnet
  count          = local.vpc_enabled && local.public2_en ? 1 : 0
  subnet_id      = aws_subnet.public2_inzone[0].id
  route_table_id = aws_route_table.public[0].id
}
