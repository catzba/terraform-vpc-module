output "subnet_public_id" {
  description = "List of IDs of instances"
  value       = local.subnet_id[0]
}

output "subnet_private_id" {
  description = "List of IDs of instances"
  value       = local.subnet_id[1]
}

output "vpc_id" {
  description = "List of IDs of instances"
  value       = aws_default_vpc.default[0].id
}

