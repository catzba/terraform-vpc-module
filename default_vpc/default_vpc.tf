#-----------------------------------------------------------------------------------------------------
# VPC
#-----------------------------------------------------------------------------------------------------

resource "aws_default_vpc" "default" {
  count = var.default_vpc_enabled ? 1 : 0
  tags = {
    Name = "Default VPC"
  }
}

#-----------------------------------------------------------------------------------------------------
# Default SUBNETS
#-----------------------------------------------------------------------------------------------------

data "aws_subnet_ids" "default" {
  count  = var.default_vpc_enabled ? 1 : 0
  vpc_id = aws_default_vpc.default[0].id
}

locals {
  subnet_id = tolist(data.aws_subnet_ids.default[0].ids)
}
