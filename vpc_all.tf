#-----------------------------------------------------------------------------------------------------
# VPS's
#-----------------------------------------------------------------------------------------------------
module "default_vpc" {
  source   = "./default_vpc"
  for_each = local.default_vpc
  # module variables
  default_vpc_enabled = lookup(each.value, "default_vpc_enabled", false)
  project_name        = var.project_name
  environment         = var.environment
  aws_region          = var.aws_region
}

module "prod_vpc" {
  source   = "./prod_vpc"
  for_each = local.prod_vpc
  # module variables
  prod_vpc_enabled = lookup(each.value, "prod_vpc_enabled", false)
  project_name     = var.project_name
  environment      = var.environment
  aws_region       = var.aws_region
}



#-----------------------------------------------------------------------------------------------------
# LOCALS
#-----------------------------------------------------------------------------------------------------
locals {
  default_vpc = var.default_vpc_enabled == true ? var.default_vpc : {}
  prod_vpc    = var.prod_vpc_enabled == true ? var.prod_vpc : {}
  vpc_id             = (var.default_vpc_enabled == true ? module.default_vpc["vpc"].vpc_id : module.prod_vpc["vpc"].vpc_id)
  subnet_private_id  = (var.default_vpc_enabled == true ? module.default_vpc["vpc"].subnet_private_id : module.prod_vpc["vpc"].subnet_private_id)
  subnet_public_id   = (var.default_vpc_enabled == true ? module.default_vpc["vpc"].subnet_public_id : module.prod_vpc["vpc"].subnet_public_id)
  subnet_private_id2 = (var.default_vpc_enabled == true ? module.default_vpc["vpc"].subnet_public_id : module.prod_vpc["vpc"].subnet_private_id2)
  subnet_public_id2  = (var.default_vpc_enabled == true ? module.default_vpc["vpc"].subnet_private_id : module.prod_vpc["vpc"].subnet_public_id2)

}




