#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------
variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# Enable VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "default_vpc" {
  default = {
    vpc = {
      default_vpc_enabled = true
    }
  }
}

variable "prod_vpc" {
  default = {
    vpc = {
      prod_vpc_enabled = true
    }
  }
}

variable "default_vpc_enabled" {
  default = false
}

variable "prod_vpc_enabled" {
  default = false
}
